'use strict';
/*
 * ページ内リンクスクロール
 */

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

$.fn.pageScroll = function (options) {
  return $(this).each(function () {
    new $.pageScroll(this, $.extend({
      'position': 0,
      //停止位置の調整
      'fnc': undefined //beforeScroll関数

    }, options));
  });
};

$.pageScroll = function (elem, options) {
  this.$elem = $(elem);
  this.options = options;
  this.fnc = this.options['fnc'];
  this.afterfnc = this.options['afterfnc'];
  this.href = this.$elem.attr("href");
  this.href = this.href == "#" || this.href == "" ? 'html' : this.href;

  if (this.href.split('#').length !== 1) {
    this.href = '#' + this.href.split('#')[1];
  } else if (this.href != 'html') {
    return true;
  }

  this.speed = 500;
  this.setEvent();
};

$.pageScroll.prototype = {
  setEvent: function setEvent() {
    var self = this;
    self.$elem.click(function () {
      if (self.fnc !== undefined) {
        self.fnc();
      }

      return self.startScroll();
    });
  },
  startScroll: function startScroll() {
    var self = this;
    this.target = $(this.href);

    if (this.target.length > 0) {
      this.position = this.target.offset().top + this.options.position;
      $('html, body').animate({
        scrollTop: this.position
      }, self.speed, 'swing');
      return false;
    } else {
      return true;
    }
  }
};
/*
 * 画面内に要素を固定、一定要素まで来たらそこへ固定（pagetopなどに使用）
 */

$.fn.fixedElement = function (options) {
  return $(this).each(function () {
    new fixedElement(this, $.extend({
      'position': 'bottom',
      //停止位置の調整
      'fixedelement': 'footer',
      //停止させたい要素名
      'adjust': {
        window: 768,
        num: 10 //調整値

      }
    }, options));
  });
};

var fixedElement =
/*#__PURE__*/
function () {
  function fixedElement(elem, options) {
    var _this = this;

    _classCallCheck(this, fixedElement);

    this._$btn = $(elem).hide();
    this._options = options;
    this._BottomPos = parseInt(this._$btn.css('bottom'));
    $(window).on('scroll', function () {
      _this.chkPos();
    });
  }

  _createClass(fixedElement, [{
    key: "chkPos",
    value: function chkPos() {
      var WindowHeight = $(window).height();
      var PageHeight = $(document).height();
      var ScrollTop = $(window).scrollTop();
      var footerHeight = $(this._options.fixedelement).height();
      var MoveTopBtn = WindowHeight + ScrollTop + footerHeight - PageHeight;
      var chkfooter = ScrollTop >= PageHeight - WindowHeight - footerHeight + this._BottomPos;
      var adjust = !window.matchMedia('(max-width: ' + this._options.adjust.window + 'px)').matches ? this._options.adjust.num : 0;

      if (this._options.position == 'top') {
        adjust = adjust + this._$btn.height();
      } else if (this._options.position == 'center') {
        adjust = adjust + this._$btn.height() / 2;
      }

      if (ScrollTop > 200 && !this._$btn.is(':visible')) {
        this._$btn.fadeIn();
      } else if (ScrollTop < 200 && this._$btn.is(':visible')) {
        this._$btn.fadeOut();
      }

      if (chkfooter) {
        this._$btn.css({
          bottom: MoveTopBtn + adjust
        });
      } else {
        this._$btn.css({
          bottom: this._BottomPos
        });
      }
    }
  }]);

  return fixedElement;
}(); //URLにハッシュがついていればその位置までアニメーションでスクロールする


var scrollAfterLoading = function scrollAfterLoading() {
  _classCallCheck(this, scrollAfterLoading);

  var hash = window.location.hash;
  $('html, body').css('visibility', 'hidden'); //一旦非表示に

  if (hash.length < 2) {
    $('html, body').css('visibility', '');
  } else {
    setTimeout(function () {
      $('html, body').scrollTop(0).css('visibility', '').delay(500).queue(function () {
        var position = $(hash).offset().top;
        $('html, body').animate({
          scrollTop: position
        }, 1000, 'swing').dequeue();
      });
    }, 0);
  }
}; //タブ切り替え


var tabContent =
/*#__PURE__*/
function () {
  function tabContent() {
    _classCallCheck(this, tabContent);

    var self = this;
    $('.tab').each(function () {
      self.setTab(this);
    });
  }

  _createClass(tabContent, [{
    key: "setTab",
    value: function setTab(elem) {
      var self = this;
      var $elem = $(elem);
      var $tab = $elem.find('.tab__link');
      var $contain = $elem.find('.tab__container');
      var startnum = $elem.data('tabStart') === void 0 ? 0 : $elem.data('tabStart');
      $tab.on('click', function (e) {
        e.preventDefault();

        if (!!$(this).data('tabTarget')) {
          $tab.removeClass('tab__link--focus');
          $(this).addClass('tab__link--focus');
          $contain.hide().filter('[rel="' + $(this).data('tabTarget') + '"]').show();
        }

        ;
      });
      $tab.eq(startnum).trigger('click');
    }
  }]);

  return tabContent;
}(); //文字サイズ変更


var changeFontSize = function changeFontSize() {
  _classCallCheck(this, changeFontSize);

  var $btn = $('.fontsizechange__link');
  $('.fontsizechange__link').on('click', function (e) {
    e.preventDefault();
    $btn.removeClass('fontsizechange__link--focus');
    $(this).addClass('fontsizechange__link--focus');
    $('html').removeClass().addClass($(this).data('fontSize'));
    $.cookie('fontSize', $(this).data('fontSize'));
  });

  if ($.cookie('fontSize') !== void 0) {
    $('.fontsizechange__link[rel="' + $.cookie('fontSize') + '"]').trigger('click');
  }

  ;
}; //フォーム周り


var formSetting = function formSetting() {
  _classCallCheck(this, formSetting);

  this.$form = $('form');
};

(function () {
  $('.footer__pagetop__link').fixedElement({
    fixedelement: '.footer__wrapper',
    position: 'bottom',
    adjust: {
      window: '1px',
      num: -20
    }
  }).pageScroll();
  new tabContent();
  new formSetting(); //new scrollAfterLoading();
})();