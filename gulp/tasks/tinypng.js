const tinypng = require('gulp-tinypng-compress');
const tinypngOptions = {
    key: 'CLg1MH0ysFhKFq4Bcl2zfzmDDFrddB8t',
    cached: true
};

module.exports = () => {
    $.gulp.task('png', () => {
        return $.gulp
            .src($.path.src.img + '**/*.png')
            .pipe($.debug())
            .pipe(tinypng(tinypngOptions))
            .pipe($.gulp.dest($.path.dev.img));
    });
    $.gulp.task('png:prod', () => {
        return $.gulp
            .src($.path.dev.img + '**/*.png')
            .pipe($.debug())
            .pipe(tinypng(tinypngOptions))
            .pipe($.gulp.dest($.path.prod.img));
    });
};
