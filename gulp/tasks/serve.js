module.exports = () => {
    $.gulp.task('serve', () => {
        $.browserSync.init({
            //proxy: "127.0.0.1:8001"
            server: {
                baseDir: 'webroot/'
            },
            startPath: '/'
        });
    });
};
