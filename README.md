# pugtemplate
gulp+pug+sass+babelで静的サイトを効率よく構成する為のテンプレート一式です。


## 始め方
bitbacketからファイル一式をcloneし、新しくディレクトリを作成し、コピーして使用してください。

`npm install --save-dev`で必要なプラグインが読み込まれます。あとは`npm run gulp`で動きます。



## ファイル構成

### webroot
`npm run gulp`でブラウザに表示されるディレクトリです。

resourcesのcompile結果がここへ出力されます。

webroot自体は画像やJsのプラグインファイルを追加する際だけ編集し、その他は全てresourcesディレクトリ内で作業します。

### product
`npm run gulp build`で納品用ファイルとしてcssからmapファイルの指定を取り除いたり、ファイル類を圧縮したものが格納されます。

### resources
実際の作業ディレクトリです。

	■es...ページ全体で使用するjavascript
	└app.js
	
	■pug...最初に_が付くフォルダ及びファイルはhtmlにcompileされない
	├_partials...サイト設計に使用するtemplate類
	|	├_config.pug...サイト名やdescriptionなどサイト全体の設定を記述
	|	├_data.pug...サイト内で使用するまとまったデータ類を記述
	|	├_favicons.pug...head内に入るfavicon類の設定
	|	├_footer.pug...footerファイル
	|	├_ga.pug...GoogleAnalyticsのscript
	|	├_head.pug...サイト全体のheadタグの中身を記述
	|	├_header.pug...headerファイル
	|	├_layout-page.pug...下層ページに使用するtemplate
	|	├_layout.pug...サイト全体のtemplate
	|	├_localmixin.pug...サイト固有で使用するmixin
	|	└_mixin.pug...汎用mixin(OGPタグなど)
	|
	└sample...実際にhtmlとして出力されるファイル類(この場合webroot下にsampleディレクトリが生成される)
		├page_sample.pug
		└page_sample2.pug


	■sass...最初に_が付くフォルダ及びファイルはcssにcompileされない
	├top.scss...トップページでのみ使用するスタイルを記述
	├app.scss...下記全てのsassのまとめ及びタグそのものへのスタイル、cssハックなどを記述
	|
	├_components...ページまたは要素のスタイルを記述
	|	├_content.scss...別途ファイルを分ける程でも無いページごとのスタイルを記述
	|	├_button.scss...ボタン
	|	├_footer.scss...フッタ
	|	├_form.scss...フォーム周り
	|	├_header.scss...ヘッダ
	|	├_icon.scss...アイコン
	|	├_layout.scss...全体のレイアウト
	|	├_list.scss...リスト
	|	├_nav.scss...ナビゲーション(グローバルナビ、ローカルナビ)
	|	├_page.scss...サイト内汎用スタイル
	|	├_tab.scss...タブ
	|	├_table.scss...テーブル
	|	└_tag.scss...タグ
	|
	└_library...色設定や関数などscssの構成に使用するプログラム類を記述
		├_animation.scss...使用するアニメーションタイムラインを記述
		├_colors.scss...使用する色を変数として記述
		├_fonts.scss...フォントファイルの読み込み
		├_mixin.scss...sass内で使用する関数や変数などを記述
		└_typo.scss...テキストスタイルの設定を記述


--
## jQueryプラグイン
### pageScroll
#### ページ内リンク時にアニメーションでスクロールします
> $('a').pageScroll(option);

#### option
|プロパティ||説明|
|:---|:---|:---|
|**potision**|number|アニメーションの停止位置を調整します。（固定ヘッダなどがある場合に使用）|
|**speed**|number|アニメーションの再生速度（デフォルトは500）|
|**beforeScroll**|function|スクロール開始前に実行される関数|
|**afterScroll**|function|スクロール終了後に実行される関数|

### fixedElement
#### 要素を画面に固定し、一定要素まで来たらそこでストップさせます（トップへ戻るボタンなどに使用）
> $('固定させたい要素').fixedElement(option);

#### option

|プロパティ||説明|
|:---|:---|:---|
|**fixedelement**|selector|固定先の要素（.footerなど）|
|**potision**|top/center/bottom|fixedelementのどの位置で固定するか（デフォルトはbottom）|

### scrollAfterLoading
#### URLにハッシュ（#）が付いている場合、アニメーションスクロールしながら表示
pageScrollを使用しています。
>(() => {
>new scrollAfterLoading();
>});

### tabContent
#### タブ切り替え
index.pug内にsampleがあります
>new tabContent();

### changeFontSize
#### 文字サイズ変更


### spMenu
#### スマホメニューを表示する
 a data-spmenuをクリックするとbody class="activemenu"がtoggleClassするので後はcssでお好みに。
activemenuが付与されるとspMenu.open、削除されるとspMenu.closeイベントトリガーがbodyに対して発行されます。
>new spMenu();


--

## 便利なmixin
scssやpug内の_mixinファイルにはちょっと便利な関数を入れています。

## SCSS編
### mq($size(sm/md/lg), $width: max(,min));
#### メディアクエリを生成します。
$sizeにはpxなどの数値を入れる事も出来ますが、sm,md,lgで規定値を呼び出して使う方が多いでしょう。

規定値は同じ_mixin.scssファイル内で以下の様に設定されていますので適宜変更してください。

	/// Breakpoint
	$screen: (
	  sm: 640px,
	  md: 960px,
	  lg: 1400px
	) !default;
	
 使用するときは次のように使います。
 
 	.sp-Only;
 		display: none;
	 	@include mq(sm, min){
	 		display: block;
	 	}
 	}
 
-

### getArw($color, $weight(light,medium,bold,bla), $rotate(top,bottom,right,left), $round(true/false));
#### 色や太さ、角丸を設定したsvgの矢印データを返します。
以下のようにcontentやbackground-imageに使用できます。(疑似要素はサイズ指定が出来ない為、background-imageで指定してbackground-size: contain;で調整する方が使いやすいです。)
	
	.arw{
	    background-image: getArw(#FFCC00, bla, left);
	    background-repeat: no-repeat;
	    background-size: 20px;
	    background-position: 0 50%;
	    padding-left: 25px;
	}
	.arw-2{
	    background: rgb(177, 81, 81);
	    padding: 10px;
	    display: flex;
	    justify-content: space-between;
	    color: #fff;
	    &:after{
	        content: getArw(rgb(255, 255, 255), light, right);
	        display: inline-block;
	        width: 15px;
	        height: 15px;
	    }
	}
-

### getblankicon($color: #000000, $round: true)
#### 色を設定した別窓アイコンを返します。

	

-

## pug編
### imgsrcset({src: $String, [alt: $String, class: $String, id: $String]}, set2x=false);
#### ratina用画像を設定したimgタグを出力します。
srcのみ必須で残りは任意で指定可能です。

画像を自動で生成するわけではないので、retina用の画像をファイル名@2x.pngという名前で用意しておく必要があります。

	p
		+imgsrcset({src:"/assets/img/img_creditcard.png", alt:"クレジットカード一覧", class: "card"})
		
これをhtmlに変換すると以下のタグが出力されます

	<p>
		<img src="/assets/img/img_creditcard.png" srcset="/assets/img/img_creditcard@2x.png 2x" alt="クレジットカード一覧" class="card" />
	</p>